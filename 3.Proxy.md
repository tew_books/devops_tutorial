
---

TeamEverywhere Nodejs Guide
*Updated at 2019-12-01*

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [TeamEverywhere](https://www.team-everywhere.com/)*

- [Proxy란?](#Proxy란?)
- [Why_Proxy?](#Why_Proxy?)
- [NginX](#NginX)


## Proxy란?
    - 서버와 클라이언트 간에 대리로 통신을 수행하는 것을 가리켜 Proxy, 이런 역할을 수행하는 서버를 Proxy 서버라고 합니다.
<img src="without_proxy.png" width="640" height="270" />
<img src="with_proxy.png" width="965" height="270" />


## Why_Proxy?
    - 퍼포먼스
        - 프록시 서버는 일부 데이터를 캐시로 저장하여, 유저 입장에서 빠르게 서버 기능을 사용할 수 있도록 합니다.
        - 예를 들어 원 서버가 미국에 있고 프록시 서버가 한국에 있을 경우, 
          한국의 사용자들은 미국 서버에 접속하지 않더라도 한국에 있는 프록시 서버에서 제공하는 캐시 데이터를 통해 빠르게 홈페이지에 접속할 수 있습니다.
    - 트래픽 관리
        - 서버에 직접적으로 연결되는 트래픽을 줄임으로써, 병목현상을 줄일 수 있습니다.
    - 보안
        - 유저가 서버 데이터에 직접적으로 접근하지 못하게 함으로써, 보안수준을 향상시킬 수 있습니다.        
    - 지역제한을 우회하기 위해 사용하기도 합니다.
        - VPN을 사용하지 않아도 됩니다.


## NginX
    - 웹 서버 소프트웨어로, 서버를 만들 수 있는 소프트웨어 입니다.
    - 러시아의 이고르 시쇼브가 아파치의 단점을 극복하기 위해서 만든 서버 프로그램인 만큼, 상당히 가볍고 빠릅니다.
    - 원래 NginX 설정을 하는 것은 복잡하고, 공부할 것도 많습니다.
    - 하지만 우리에게는 Docker가 있죠.
    - 우리는 Docker를 통해 간단하게 NginX Proxy Server를 구현할 예정입니다.
<img src="nginx_proxy.png" width="965" height="583" />

<!-- END doctoc generated TOC please keep comment here to allow auto update -->
